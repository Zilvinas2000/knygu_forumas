<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Illustration extends Model
{
    public function creator() {
    	return $this->hasOne(
    		'App\User',
    		'id',
    		'created_by');
    }
    protected $table = 'illustrations';
}