<?php

namespace App\Http;

use App\Category;
use Illuminate\Database\Eloquent\Model;

class BookHelpers 
{
	public static function getCategories()
	{
		$categories = Category::all();
		return $categories;
	}
	// public static function getIllustrations() 
	// {
 //        $illustrations = Illustration::orderBy('id', 'desc')->take(5)->get();
 //    	return $illustrations;
 //    }
}