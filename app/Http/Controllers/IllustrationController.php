<?php

namespace App\Http\Controllers;

use App\Illustration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IllustrationController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
        $this->middleware('admin')->except('store', 'index', 'show', 'create');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $illustrations = Illustration::all();
        return view('illustrations.index', ['illustrations' => $illustrations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('illustrations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ill_name' => 'required|max:40',
            'image_url' => 'required',
        ]);
        $illustration = new Illustration;
        $illustration->ill_name = $request->input('ill_name');
        $illustration->image_url = $request->input('image_url');
        $illustration->created_by = Auth::user()->id;
        $illustration->save();

        $request->session()->flash('success', 'Nauja iliustracija sėkmingai įkelta!');
        return redirect()->route('illustrations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Illustration  $illustration
     * @return \Illuminate\Http\Response
     */
    public function show(Illustration $illustration)
    {
        return view('illustrations.show', ['illustration' => $illustration]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Illustration  $illustration
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $illustration = Illustration::find($id);
        return view('illustrations.edit', compact('illustration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Illustration  $illustration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ill_name' => 'required|max:45',
            'image_url' => 'required',
        ]);
        $illustration = Illustration::find($id);
        $illustration->ill_name = $request->input('ill_name');
        $illustration->image_url = $request->input('image_url');
        $illustration->created_by = Auth::user()->id;
        $illustration->save();

        $request->session()->flash('success', 'Iliustracija sėkmingai pakeista!');
        return redirect()->route('illustrations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Illustration  $illustration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Illustration $illustration)
    {
        $illustration->delete();
        return redirect()->route('illustrations.index');
    }
}