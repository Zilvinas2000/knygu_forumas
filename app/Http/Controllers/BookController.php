<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except('index', 'show');
        $this->middleware('admin')->except('store', 'index', 'show', 'create');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('created_at', 'desc')->get();
        // Book::all();
        $count = count($books);
        // dd($count);
        return view ('books.index', ['books' => $books], compact('count'));
    }

    private function count($id) {
        $count = Book::where('id', $id)->count();
        return $count;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view ('books.create', compact('categories'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'author' => 'required',
            'image_url' => 'required',
            'year' => 'required|integer',
            'pages' => 'required|integer',
            'description' => 'required|min:3',
            'category' => 'required',
        ]);

        $book = new Book;
        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $book->image_url = $request->input('image_url');
        $book->year = $request->input('year');
        $book->pages = $request->input('pages');
        $book->description = $request->input('description');
        $book->category_id = $request->input('category');
        $book->created_by = Auth::user()->id;
        $book->save();
        $request->session()->flash('success', 'Naujas knygos aprašymas sėkmingai sukurtas!');
        return redirect()->route('books.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book, Request $request)
    {
        $comments = Comment::where('book_id',$book->id)->get();
        return view('books.show ', ['book' => $book, 'comments' => $comments]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $book = Book::find($id);
        //dd($book);
        return view('books.edit', compact('book', 'categories'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'author' => 'required',
            'image_url' => 'required',
            'year' => 'required|integer',
            'pages' => 'required|integer',
            'description' => 'required|min:3',
            'category' => 'required',
        ]);

        $book = Book::find($id);
        $book->title = $request->input('title');
        $book->author = $request->input('author');
        $book->image_url = $request->input('image_url');
        $book->year = $request->input('year');
        $book->pages = $request->input('pages');
        $book->description = $request->input('description');
        $book->category_id = $request->input('category');
        $book->created_by = Auth::user()->id;
        $book->save();
        return redirect()->route('books.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
            return redirect()->route('books.index');
    }
}