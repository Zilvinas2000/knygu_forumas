<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/books', 'BookController');
Route::resource('/comments', 'CommentController', ['only' => [ 'index', 'store', 'destroy', 'create']]);
Route::get('/contacts', 'ContactController@index')->name('contacts');
Route::get('filters/{id}', 'FilterController@show')->name('category_filter');
Route::resource('/users', 'UserController');
Route::resource('/illustrations', 'IllustrationController');
Route::resource('/quotes', 'QuoteController');
Route::resource('/', 'QuoteController');