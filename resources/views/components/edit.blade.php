@if(Auth::user())
	@if(Auth::user()->role == 'admin')
		<div class="margin-top">
			<a href="{{ route($route, $id) }}" class="btn btn-block btn-warning">
				{{ $name }}
			</a>
		</div>
	@endif
@endif