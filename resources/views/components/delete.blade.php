@if(Auth::user())
	@if(Auth::user()->role == 'admin')	
		<form method="POST" action="{{ route($route, $id) }}">
			@if($book->id)
			<input type="hidden" name="book_id" value="{{ $book->id }}" />
			@endif
        	{{ method_field("DELETE") }}
    		{{ csrf_field() }}
    		<button class="btn btn-danger btn-block">{{ $name }}</button>
		</form>
	@endif
@endif