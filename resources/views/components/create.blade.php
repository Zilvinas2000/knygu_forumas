@if(Auth::user())
	<div class="margin-top">
		<a href="{{ route($route) }}" class="btn btn-lg btn-block btn-success">
    		{{ $name }}
		</a>
	</div>
@endif