@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Redaguoti knygos aprašymą</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('books/'.$book->id) }}">
                        <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Knygos pavadinimas</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title', $book->title) }}">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
                            <label for="author" class="col-md-4 control-label">Autorius</label>
                            <div class="col-md-6">
                                <input id="author" type="text" class="form-control" name="author" value="{{ old('author', $book->author) }}" autofocus>
                                @if ($errors->has('author'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('author') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image_url') ? ' has-error' : '' }}">
                            <label for="image_url" class="col-md-4 control-label">Knygos viršelio nuotraukos nuoroda</label>
                            <div class="col-md-6">
                                <input id="image_url" type="text" class="form-control" name="image_url" value="{{ old('image_url', $book->image_url) }}" >
                                @if ($errors->has('image_url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image_url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                            <label for="year" class="col-md-4 control-label">Leidimo metai</label>
                            <div class="col-md-6">
                                <input id="year" type="text" class="form-control" name="year" value="{{ old('year', $book->year) }}">
                                @if ($errors->has('year'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('year') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pages') ? ' has-error' : '' }}">
                            <label for="pages" class="col-md-4 control-label">Puslapių skaičius</label>
                            <div class="col-md-6">
                                <input id="pages" type="text" class="form-control" name="pages" value="{{ old('pages', $book->pages) }}">
                                @if ($errors->has('pages'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pages') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Aprašymas</label>
                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control resize" name="description">{{ old('description', $book->description) }}
                                </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-4 control-label">Kategorija</label>
                            <div class="col-md-6">
                                <select id="category" type="text" class="form-control" name="category">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}"
                                                @if($book->category_id == $category->id)
                                                selected
                                                @endif
                                        >{{ $category->category }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @component('books.components.edit') @endcomponent
                            </div>
                        </div>
                        @component('components.back') @endcomponent
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection