@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-12 margin-top">
            @component('components.delete', ['book' => $book, 'route' => 'books.destroy', 'id' => $book->id, 'name' => 'Ištrinti knygą']);
            @endcomponent
            @component('components.edit', ['route' => 'books.edit', 'id' => $book->id, 'name' => 'Redaguoti']);
            @endcomponent
        </div>
    </div>
    <div class="row book-list-reset">    
        <ul class="list-group col-md-12 col-sm-12">
            <li class="list-group-item clearfix book-list">
                <div class="col-md-4 col-sm-1">
                    <img class="img-responsive" src="{{$book->image_url}}">
                    Sukūrė: {{ $book->creator->nickname }}
                </div>
                <div class="col-md-8">
                    <p class="pav">{{$book->title}}</p>
                    <p></p>
                    Autorius: {{$book->author}}
                    <p></p>
                    Išleista {{$book->year}} metais
                    <p></p>
                    {{$book->pages}} puslapių (-iai)
                    <p></p>
                    {{$book->description}}
                    <p></p>
                    Kategorija - {{$book->category->category}}
                </div>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Komentarai :</h3>
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 margin_bottom">
            <form method="POST" action="{{ route('comments.store') }}">
                {{ csrf_field() }}
                <input type="hidden" name="book_id" value="{{ $book->id }}" />
                <div class="col-md-2 col-sm-2 col-xs-2 comment-right">
                    <input class="comment-box" id="name" type="text" name="name" placeholder="Jūsų vardas" value="{{ old('name') }}">
                    @if ($errors->has('comments'))
                        <span class="help-block">
                            <strong>{{ $errors->first('comments') }}</strong>
                        </span>
                    @endif
                </div>
                <textarea id="comment" name="comment" rows="4" class="col-md-8 col-sm-8 col-xs-8 comment-box" placeholder='Komentaras'>{{ old('comment') }}</textarea>
                @component('books.components.comment') @endcomponent
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul>
                @foreach($comments as $comment)
                    <li class="comma clearfix">
                        <div class="comma-left">
                            <div class="comment-name">{{ $comment->name }}</div>
                            <img src="{{ asset('img/default-user.png')}}" alt="picture" class="comment-face">
                            <div class="comment-date">{{ $comment->created_at }}</div>
                        </div>
                        <div class="comment-comment comma-center"><i class="fa fa-commenting-o" aria-hidden="true"></i> {{ $comment->comment }}</div>
                        <div class="comma-right"></div>
                    </li>
                    @component('components.delete', ['book' => $book, 'route' => 'comments.destroy', 'id' => $comment->id, 'name' => 'Ištrinti netinkamą komentarą']);
                    @endcomponent
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection