@extends('layouts.main')

@section('turinys')
<div class="container">
    @if(Session::get('success'))
    <div class="row">
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="cage clearfix">
                <div class="one">
                    <h3>Perskaitytų knygų aprašymai</h3>
                    <p>Visi geriausių ir mylimiausių knygų aprašymai bei skaitytojų komentarai! Aprašykite savo perskaitytą knygą, įkelkite savo mintis, knygos viršelio nuotrauką ir diskutuokite, dalinkitės mintimis, įžvalgomis, nuomonėmis su bendraminčiais!</p>
                </div>
                <div class="two">
                    <img class="contacts-img" src="{{ asset('img/writer.jpg')}}" alt="img">
                </div>
            </div>                
        </div>    
    </div>
    <div class="row book-list-reset">
        <div class="col-md-12 col-sm-12">
            <h3>Iš viso : <span>{{ $count }}</span> knygų aprašymų(-ai) !</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            @component('components.create', ['route' => 'books.create', 'name' => 'Sukurti naują knygos aprašymą']);
            @endcomponent
        </div>
    </div>
    <div class="row book-list-reset">
        <ul class="list-group col-md-12 col-sm-12">
            @foreach($books as $book)
                <li class="list-group-item clearfix book-list">
                    <div class="col-md-1 col-sm-1">
                        <a href="{{ route('books.show', $book->id) }}">
                            <img class="img-responsive" src="{{$book->image_url}}">
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <strong>{{$book->title}}</strong>
                        <p></p>
                        Autorius: {{$book->author}}
                        <p></p>
                        Išleista {{$book->year}} metais
                        <p></p>
                        Kategorija - {{$book->category->category}}
                    </div>
                    <div class="col-md-7 col-sm-7">
                        {{ substr($book->description, 0, 450) }}
                        <a href="{{ route('books.show', $book->id) }}">
                            ...Skaityti daugiau
                        </a>
                    </div>
                    <div class="col-md-2 col-sm-7">
                        Sukūrė: {{ $book->creator->nickname }}
                        @component('components.delete', ['book' => $book, 'route' => 'books.destroy', 'id' => $book->id, 'name' => 'Ištrinti knygą']);
                        @endcomponent
                        @component('components.edit', ['route' => 'books.edit', 'id' => $book->id, 'name' => 'Redaguoti']);
                        @endcomponent
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>
@endsection