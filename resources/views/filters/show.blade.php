@extends('layouts.main')

@section('turinys')
<div class="container">
    @if(Session::get('success'))    
    <div class="row">
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    </div>
    @endif
    @if( count($books) != 0 )
    <div class="row">
        <div class="col-md-12">
            <h3>{{ $books[0]->category->category }} <span>({{ $count }})</span> knygų aprašymų(-ai)</h3>
        </div>
    </div>
    <div class="row book-list-reset">
        <ul class="list-group col-md-12">
            @foreach($books as $book)
                <li class="list-group-item clearfix book-list">
                    <div class="col-md-1">
                        <a href="{{ route('books.show', $book->id) }}">
                            <img class="img-responsive" src="{{$book->image_url}}">
                        </a>
                    </div>
                    <div class="col-md-2">
                        <strong>{{$book->title}}</strong>
                        <p></p>
                        Autorius: {{$book->author}}
                        <p></p>
                        Išleista {{$book->year}} metais
                        <p></p>
                        Kategorija - {{$book->category->category}}
                    </div>
                    <div class="col-md-7">
                        {{ substr($book->description, 0, 450) }}
                        <a href="{{ route('books.show', $book->id) }}">
                            ...Skaityti daugiau
                        </a>
                    </div>
                    <div class="col-md-2">
                        Sukūrė: {{ $book->creator->nickname }}
                        @component('components.delete', ['book' => $book, 'route' => 'books.destroy', 'id' => $book->id, 'name' => 'Ištrinti knygą']);
                        @endcomponent
                        @component('components.edit', ['route' => 'books.edit', 'id' => $book->id, 'name' => 'Redaguoti']);
                        @endcomponent
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    @else
    <h3>Jūsų pasirinkta kategorija tuščia</h3>
    @endif
</div>
@endsection