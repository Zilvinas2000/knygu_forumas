@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Įkelti naują iliustraciją</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('illustrations') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('ill_name') ? ' has-error' : '' }}">
                            <label for="ill_name" class="col-md-4 control-label">Iliustracijos pavadinimas</label>
                            <div class="col-md-6">
                                <input id="ill_name" type="text" class="form-control" name="ill_name" value="{{ old('ill_name') }}" autofocus>
                                @if ($errors->has('ill_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ill_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image_url') ? ' has-error' : '' }}">
                            <label for="image_url" class="col-md-4 control-label">Iliustracijos nuoroda</label>
                            <div class="col-md-6">
                                <input id="image_url" type="text" class="form-control" name="image_url" value="{{ old('image_url') }}" >
                                @if ($errors->has('image_url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image_url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @component('illustrations.components.create') @endcomponent
                            </div>
                        </div>
                        @component('components.back') @endcomponent
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection