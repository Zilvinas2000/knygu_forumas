@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-12 margin-top">
            @if(Auth::user())
                            @if(Auth::user()->role == 'admin')  
                                <form method="POST" action="{{ route('illustrations.destroy', $illustration->id) }}">
                                    {{ method_field("DELETE") }}
                                    {{ csrf_field() }}
                                    <button class="btn btn-danger btn-block">Trinti</button>
                                </form>
                            @endif
                        @endif
            @component('components.edit', ['route' => 'illustrations.edit', 'id' => $illustration->id, 'name' => 'Redaguoti']);
            @endcomponent
            @component('components.back') @endcomponent
            <img class="img-responsive img-styles-second" src="{{$illustration->image_url}}">
        </div>
    </div>   
</div>
@endsection