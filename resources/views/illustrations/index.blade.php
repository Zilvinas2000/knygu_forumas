@extends('layouts.main')

@section('turinys')
<div class="container">
    @if(Session::get('success'))
    <div class="row">
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="cage clearfix">
                <div class="one">
                    <h3>Skaitytojų sukurtos iliustracijos</h3>
                    <p>Visi tekstų kūrėjai ir rašytojai paprastai puikiai rašo ir dėsto savo mintį. Žodžiai liejasi, kaip iš dviejų gausybės ragų, tačiau yra viena bėda – galutinis rezultatas – 3 puslapiai ištisinio teksto juodu ant balto yra labai sunkiai skaitomas be iliustracijų. Gali būti ir tinkamas teksto formatavimas, tačiau jokios iliustracijos nebuvimas tiesiog varo į neviltį kai kuriuos skaitytojus. Jie negali sukoncentruoti dėmesio ir baigti skaityti. Jei tu mėgsti piešti ar iliustruoti mėgstamas knygas - dėk savo piešinius čia! Tai puiki proga parodyti savo sugebėjimus pasauliui ir sulaukti pripažinimo!</p>
                </div>
                <div class="two">
                    <img class="contacts-img" src="{{ asset('img/painter.jpg')}}" alt="img">
                </div>
            </div>                
        </div>    
    </div>
    <div class="row">
        <div class="col-md-12">
            @component('components.create', ['route' => 'illustrations.create', 'name' => 'Įkelti naują knygos iliustraciją']);
            @endcomponent
        </div>
    </div>
    <div class="row book-list-reset">
        <ul class="list-group col-md-12">
            @foreach($illustrations as $illustration)
                <li class="col-md-4 list-group-item clearfix book-list">
                    <div>
                        <a href="{{ route('illustrations.show', $illustration->id) }}">
                            <img class="img-responsive img-styles" src="{{$illustration->image_url}}">
                        </a>
                        <p class="pav">Pavadinimas: {{$illustration->ill_name}}</p>
                        <p></p>
                        Sukūrė: {{ $illustration->creator->nickname }}
                    </div>
                    @if(Auth::user())
                        @if(Auth::user()->role == 'admin')  
                            <form method="POST" action="{{ route('illustrations.destroy', $illustration->id) }}">
                                {{ method_field("DELETE") }}
                                {{ csrf_field() }}
                                <button class="btn btn-danger btn-block">Trinti</button>
                            </form>
                        @endif
                    @endif
                    @component('components.edit', ['route' => 'illustrations.edit', 'id' => $illustration->id, 'name' => 'Redaguoti']);
                    @endcomponent
                </li>
            @endforeach
        </ul>
    </div>
</div>
@endsection