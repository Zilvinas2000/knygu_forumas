<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('/css/reset.css') }}">
	<link rel="stylesheet" href="{{ asset('css/styles.css') }}">	
	<link href="https://fonts.googleapis.com/css?family=Bellefair|Open+Sans&amp;subset=latin-ext" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{ asset('/css/flexslider.css') }}">
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"
  	integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  	crossorigin="anonymous"></script>
  	<script type="text/javascript" src="{{ asset('/js/jquery.flexslider.js') }}"></script>
	<title>Knygu Forumas</title>
</head>

<body>
<header>
	<div class="container">
		<div class="row">	
			<div class="col-md-12">
				<div class="brown clearfix">
					<div class="logo">
						<a href="{{ route('index') }}"><img class="img-logo" src="{{ asset('img/logo-small.jpg')}}" alt="logo"></a>
					</div>
					<div class="name">
						<span class="block klas">Knygų forumas.lt</span>
						<span class="block2 klas">Geriausių knygų forumas</span>
					</div>
					<div class="reg">
						<ul>
							@if (Auth::guest())
								<li><a href="{{ route('login') }}">Prisijungti</a></li>
								<li><a href="{{ route('register') }}">Registruotis</a></li>
							@else
								<li>
                                	@if(Auth::user()->role == 'admin')
                                    	<a href="{{ route('users.index', Auth::user()->id) }}">
                                        	Vartotojai
                                    	</a>
                                	@endif
                            	</li>
                            	<li>
									<a href="{{ route('users.show', Auth::user()->id) }}">
         								Profilis
                                    </a>
                                </li>
	                            <li>
	                                <a href="{{ route('logout') }}"
	                                    onclick="event.preventDefault();
	                                    document.getElementById('logout-form').submit();">
	                                    Išeiti
	                                </a>
	                            </li>
	                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                	{{ csrf_field() }}
	                                </form>
                          	@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row">	
			<div class="col-md-12">		
				<nav>
					<ul class="menu">
						<li><a href="{{ route('contacts') }}">Apie projektą</a></li>
						<li><a href="{{ route('books.index') }}">Visos knygos</a></li>
						<li>
							<a href="#">Knygos pagal žanrą</a>
								<div class="submenu clearfix">
									<div class="left">
										<ul>
											<li>
												<a href="{{ route('category_filter', 1) }}">
													<i class="fa fa-search" aria-hidden="true"></i> Detektyvai, trileriai
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 2) }}">
													<i class="fa fa-magic" aria-hidden="true"></i> Fantastika
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 3) }}">
													<i class="fa fa-sun-o" aria-hidden="true"></i> Grožinė literatūra
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 4) }}">
													<i class="fa fa-history" aria-hidden="true"></i> Istorinė literatūra
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 5) }}">
													<i class="fa fa-superscript" aria-hidden="true"></i> Knygos paaugliams
												</a>
											</li>
										</ul>
									</div>
									<div class="left">
										<ul>
											<li>
												<a href="{{ route('category_filter', 6) }}">
													<i class="fa fa-child" aria-hidden="true"></i> Knygos vaikams
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 7) }}">
													<i class="fa fa-eye" aria-hidden="true"></i> Psichologija
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 8) }}">
													<i class="fa fa-book" aria-hidden="true"></i> Kitos knygos
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 9) }}">
													<i class="fa fa-university" aria-hidden="true"></i> Filosofija
												</a>
											</li>
											<li>
												<a href="{{ route('category_filter', 10) }}">
													<i class="fa fa-diamond" aria-hidden="true"></i> Klasika
												</a>
											</li>
										</ul>
									</div>
								</div>
						</li>
						<li><a href="{{ route('illustrations.index') }}">Iliustracijos</a></li>
					</ul>
				</nav>
				<div class="book1">
					<!-- Under submenu picture -->
				</div>
			</div>
		</div>
	</div>
</header>