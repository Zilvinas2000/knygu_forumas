	<footer>
		<div class="container">
			<div class="row">	
				<div class="col-md-12">
					<div class="brown footer">
						<p>---<span class="color">Zilvinas</span> Entertainment&#x000A9;--- 2017</p>
			    		<p>All Rights Reserved</p>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
    		$(document).ready(function(){
    			$(window).load(function() {
  					$('.flexslider').flexslider({
    					animation: "slide",
    					animationLoop: false,
    					itemWidth: 210,
    					itemMargin: 5,
    					minItems: 2,
    					maxItems: 5
  					});
				});
    		});
  		</script>
	</footer>
</body>
</html>