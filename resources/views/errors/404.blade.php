@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">                
                <div class="panel-body">
                    <h3> 
                    	<p>Klaida!</p>
                    	<p>Jūsų pasirinktas puslapis neegzistuoja arba Jūs esate neprisijungęs vartotojas.</p>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection