@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="cage clearfix">
                <div class="one">
                    <h3>Apie projektą</h3>
                <p><strong>Misija :</strong></p>
                <ul>
                    <li>Lietuvos gyventojų informacinių, kultūrinių ir laisvalaikio poreikių tenkinimas, atsižvelgiant į visų bendruomenės grupių informacinius kultūrinius bei socialinius poreikius.</li>
                    <li>Dalyvavimas žinių visuomenės kūrimo procesuose.</li>
                    <li>Knygų aprašymų, skaitytojų nuomonių kaupimas, tvarkymas, saugojimas ir teikimas Lietuvos gyventojams, atsižvelgiant į kultūrinį bei socialinį vystymą.</li>
                    <li>Knygų aprašymų, nuomonių apie knygas paslaugos teikiamos remiantis lygia naudojimosi teise visiems, nepaisant amžiaus, rasės, lyties, tautybės, socialinės padėties, politinių ar religinių įsitikinimų.</li>
                </ul>
                <p><strong>Vizija :</strong></p>
                <ul>
                    <li>Įgyti ir tobulinti informacinius, socialinius ir kultūrinius poreikius;</li>
                    <li>Naudotis savo demokratinėmis teisėmis;</li>
                    <li>Kūrybiškai dalyvauti žinių visuomenės kūrime.</li>
                </ul>
                <p><strong>Mes esame :</strong></p>
                <p>Zilvinas Entertainment&#x000A9;</p>
                </div>
                <div class="two">
                    <img class="contacts-img" src="http://www.startupremarkable.com/wp-content/uploads/2015/02/a-book-a-week-image.jpg" alt="picture">
                </div>
            </div>                
        </div>    
    </div>
</div>
@endsection