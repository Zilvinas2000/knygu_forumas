@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-12 margin-top">
            @if(Auth::user())
                @if(Auth::user()->role == 'admin')  
                    <form method="POST" action="{{ route('quotes.destroy', $quote->id) }}">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <button class="btn btn-danger btn-block">Trinti</button>
                    </form>
                @endif
            @endif
            @component('components.edit', ['route' => 'quotes.edit', 'id' => $quote->id, 'name' => 'Redaguoti']);
            @endcomponent
            @component('components.back') @endcomponent
            <div class="quote-show quote-back">
                <div class="user-image">
                    <img src="{{ asset('img/default-book.jpg')}}" alt="img">
                </div>
                {{ $quote->author }}
                {{ $quote->quote }}
            </div>
        </div>
    </div>   
</div>
@endsection