@extends('layouts.main')

@section('turinys')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Redaguoti naujieną</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('quotes/'.$quote->id) }}">
                    <input name="_method" type="hidden" value="PUT">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
                            <label for="author" class="col-md-4 control-label">Naujienos šaltinis</label>
                            <div class="col-md-6">
                                <input id="author" type="text" class="form-control" name="author" value="{{ old('author', $quote->author) }}" autofocus>
                                @if ($errors->has('author'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('author') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('quote') ? ' has-error' : '' }}">
                            <label for="quote" class="col-md-4 control-label">Naujienos turinys</label>
                            <div class="col-md-6">
                                <textarea id="quote" type="text" class="form-control" style="resize:none" name="quote">{{ old('quote', $quote->quote) }}</textarea>
                                @if ($errors->has('quote'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('quote') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Išsaugoti
                                </button>
                            </div>
                        </div>
                        @component('components.back') @endcomponent
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection