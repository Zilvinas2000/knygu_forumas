@extends('layouts.main')

	@section('turinys')
	<main>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="flexslider">
	  					<ul class="slides">
	    					<li>
	      						<img src="{{ asset('img/1.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/2.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/3.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/4.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/5.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/6.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/7.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/8.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/9.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/10.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/11.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/12.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/13.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/14.jpg')}}" alt="img" />
	    					</li>
	    					<li>
	      						<img src="{{ asset('img/15.jpg')}}" alt="img" />
	    					</li>
	    				</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="font-weight">
						<h2>Karščiausios knygų naujienos :</h2>
					</div>
			        @if(Auth::user())
			        @if(Auth::user()->role == 'admin')
			        	@component('components.create', ['route' => 'quotes.create', 'name' => 'Sukurti naujieną']);
            			@endcomponent
            		@endif
            		@endif
       				<ul>
						@foreach($quotes as $quote)
							<li>
								<div class="new-book clearfix margin-top quote-back">
									<div class="user-image">
										<img src="{{ asset('img/default-book.jpg')}}" alt="img">
									</div>
									<div class="data">
										<div class="user-info font-weight">
											<span>{{ $quote->author }}</span>
										</div>
									</div>
									<div class="book-info padding-seven">
										<p> <i class="fa fa-circle" aria-hidden="true"></i> {{ substr($quote->quote, 0, 450) }}
										<a href="{{ route('quotes.show', $quote->id) }}">
			                            ...Skaityti daugiau</p>
			                        	</a>
									</div>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>	
	</main>
	@endsection