@extends('layouts.main')

@section('turinys')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>Prisiregistravę vartotojai ( {{ $count }} )</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Slapyvardis</th>
							<th>Vardas</th>
							<th>Pavardė</th>
							<th>Gimimo data</th>
							<th>El. paštas</th>
							<th>Trinti vartotoją</th>
						</tr> 
					</thead>
					<tbody>
						@foreach ($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->nickname }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->surname }}</td>
								<td>{{ $user->birthday }}</td>
								<td>{{ $user->email }}</td>
								<td>
									<form method="POST" action="{{ route('users.destroy', $user->id) }}">
										{{ method_field("DELETE") }}
	    								{{ csrf_field() }}
	    								<button class="btn btn-danger btn-block">Ištrinti</button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection